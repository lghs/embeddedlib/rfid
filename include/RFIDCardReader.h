

#pragma once

#include <SPI.h>
#include <MFRC522.h>
#include <string>
#include "RFIDCardPayload.h"
#include "CardReadHook.h"
#include "utils.h"

using namespace std;

#define DEBUG(s)
#define MFRC522_RESET_PERIOD 10000
class RFIDCardReader {

    MFRC522 mfrc522;
    MFRC522::MIFARE_Key key;
    MFRC522::StatusCode status;
    CardReadHook* hook = 0;

    byte readbackblock[18];
    bool autoHaltAndStopCrypto;
    unsigned long lastReset = 0;

private:
    unsigned long getCardId();
    RFIDCardPayload readCard();
    int readBlock(int blockNumber, byte arrayAddress[]);
    int writeBlock(int blockNumber, byte arrayAddress[]);
    void authenticate(int blockNumber);


public:
    RFIDCardReader(int ssPin, int rstPin, CardReadHook* hook, bool autoHaltAndStopCrypto = true);
    void setup();
    void run();
    void reset();
    bool writeCard(string data);
    void haltAndStop();

};



