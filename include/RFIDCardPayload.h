

#pragma once

#include <string>

using namespace std;

class RFIDCardPayload {
public:
    RFIDCardPayload(){};
    RFIDCardPayload(long cardId, string cardContent): cardId(cardId), cardContent(cardContent){};
    long cardId = 0;
    string cardContent = "";
};



