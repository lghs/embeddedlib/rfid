

#include <stdexcept>
#include "RFIDCardReader.h"

int usableBlocks[] = {2, 4, 5, 6, 8, 9, 10, 12, 13, 14, 16, 17, 18, 20, 21, 22, 24, 25, 26, 28, 29, 30, 32, 33, 34,
                      36, 37, 38, 40, 41, 42, 44, 45, 46, 48, 49, 50, 52, 53, 54, 56, 57, 58, 60, 61, 62};

RFIDCardReader::RFIDCardReader(int ssPin,
                               int rstPin,
                               CardReadHook *hook,
                               bool autoHaltAndStopCrypto) : mfrc522(ssPin, rstPin),
                                                             hook(hook),
                                                             autoHaltAndStopCrypto(autoHaltAndStopCrypto) {
}

void RFIDCardReader::setup() {
    for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;


    Serial.println(F("Initialize rfid system"));
    SPI.begin();
    mfrc522.PCD_Init();

    Serial.print(F("Reader :"));
    mfrc522.PCD_DumpVersionToSerial();
}

RFIDCardPayload RFIDCardReader::readCard() {
    DEBUG("Read new card");
    RFIDCardPayload cardPayload;
    cardPayload.cardId = getCardId();

    DEBUG("card has id:" + to_string(cardPayload.cardId));
    //Determine the amount of blocks to read from the value stored in the block 1
    readBlock(1, readbackblock);
    int blockToUse = (int) readbackblock[0];

    DEBUG("Read block 1, blocks to use : " + to_string(blockToUse));
    //Read the data
    string out = "";
    for (int block = 0, slot = 16; block < blockToUse; block++, slot += 16) {
        readBlock(usableBlocks[block], readbackblock);
        for (int j = 0; j < 16; j++) {
            out += (char) readbackblock[j];
        }
    }
    DEBUG("Card read, payload : " + out);
    cardPayload.cardContent = out;
    return cardPayload;
}

unsigned long RFIDCardReader::getCardId() {
    if (!mfrc522.PICC_ReadCardSerial()) {
        return -1;
    }
    unsigned long hex_num;
    hex_num = mfrc522.uid.uidByte[0] << 24;
    hex_num += mfrc522.uid.uidByte[1] << 16;
    hex_num += mfrc522.uid.uidByte[2] << 8;
    hex_num += mfrc522.uid.uidByte[3];
    return hex_num;
}

int RFIDCardReader::readBlock(int blockNumber, byte arrayAddress[]) {

    authenticate(blockNumber);

    //Read block
    byte buffersize = 18;
    status = mfrc522.MIFARE_Read(blockNumber, arrayAddress, &buffersize);
    if (status != MFRC522::STATUS_OK) {
        return 4;
    } else {
        return 0;
    }
}

int RFIDCardReader::writeBlock(int blockNumber, byte *arrayAddress) {

    DEBUG("write block " + to_string(blockNumber));
    //Check if a block is a data or trailer block
    if (blockNumber > 2 && (blockNumber + 1) % 4 == 0) {
        Serial.print(blockNumber);
        Serial.println(" is a trailer block !");
        return 2;
    }

    authenticate(blockNumber);


    DEBUG("write block ");
    //Writing Block
    status = mfrc522.MIFARE_Write(blockNumber, arrayAddress, 16);
    if (status != MFRC522::STATUS_OK) {
        return 4;
    }
    return 0;
}

bool RFIDCardReader::writeCard(string dataP) {
    DEBUG("Start writing card, payload : " + dataP);
    String data = dataP.c_str();
    //Determine the amount of blocks to use and writing the value in the block 1
    int blockToUse = ceil(((float) data.length() / 16));
    DEBUG("blocks to use : " + to_string(blockToUse));
    byte buff[16];
    for (int i = 0; i < 16; i++) { buff[i] = 0; }
    buff[0] = (byte) blockToUse;
    int ret = 0;
    DEBUG("Writing block1");
    ret = writeBlock(1, buff);
    if (ret != 0) {
        throw std::runtime_error("error while writing block 1");
    }
    DEBUG("written block1");

    //Writing Data
    for (int block = 0, slot = 16; block < blockToUse; block++, slot += 16) {
        String substr = data.substring(slot - 16, slot);
        substr.getBytes(buff, 17);
        if ((ret = writeBlock(usableBlocks[block], buff)) != 0) {
            throw std::runtime_error("error while writing block " + block);
        }
    }

    DEBUG("all blocks written");

    //Check Data integrity
    readBlock(1, readbackblock);
    blockToUse = (int) readbackblock[0];
    DEBUG("read back block " + blockToUse);
    string dataCheck;
    for (int block = 0; block < blockToUse; block++) {
        readBlock(usableBlocks[block], readbackblock);
        for (int j = 0; j < 16; j++) {
            dataCheck += (char) readbackblock[j];
        }
    }

    string check = string(dataCheck.c_str()); //to remove extra chars after \0

    if (check == dataP) {
        return true;
    } else {
        return false;
    }
}

void RFIDCardReader::reset(){
    Serial.println("reset MFRC522");
    mfrc522.PCD_Init();
    this->lastReset=millis();
}

void RFIDCardReader::run() {

    try {
        if((millis() - lastReset) > MFRC522_RESET_PERIOD){
            this->reset();
        }
        // Look for new 1 cards
        if (!mfrc522.PICC_IsNewCardPresent())
            return;

        RFIDCardPayload accessCard = readCard();
        Serial.println("Card read");
        if (hook) {
            hook->onRead(accessCard);
        }

    } catch (exception &e) {
        Serial.println(("error while reading card :" + string(e.what())).c_str());
    } catch (...) {
        Serial.println("error while reading card ");
    }
    try {
        if(autoHaltAndStopCrypto){
            haltAndStop();
        }
    } catch (...) {

    }
}

void RFIDCardReader::authenticate(int blockNumber) {
    int largestModulo4Number = blockNumber / 4 * 4;
    int trailerBlock = largestModulo4Number + 3;
    MFRC522::StatusCode status = mfrc522.PCD_Authenticate(MFRC522::PICC_CMD_MF_AUTH_KEY_A, trailerBlock, &key,
                                                          &(mfrc522.uid));
    if (status != MFRC522::STATUS_OK) {
        Serial.print("authentification error ");
        Serial.println(mfrc522.GetStatusCodeName(status));
        throw runtime_error("authentication error " + to_string(status));
    }
}

void RFIDCardReader::haltAndStop() {
    DEBUG("PICC_HaltA and Stop crypto");
    mfrc522.PICC_HaltA();
    mfrc522.PCD_StopCrypto1();
}
